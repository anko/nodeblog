const express = require("express");
const expressEdge = require("express-edge");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const expressSession = require("express-session");
const connectMongo = require("connect-mongo");
const connectFlash = require("connect-flash");
const edge = require("edge.js");

const createPostController = require("./controllers/createPost");
const homePageController = require("./controllers/homePage");
const storePostController = require("./controllers/storePost");
const getPostController = require("./controllers/getPost");
const createUserController = require("./controllers/createUser");
const storeUserController = require("./controllers/storeUser");
const loginController = require("./controllers/login");
const loginUserController = require("./controllers/loginUser");
const logoutController = require("./controllers/logout");

const app = express();

mongoose.connect(
  "mongodb://localhost/node-js-blog",
  { useNewUrlParser: true }
);

app.use(connectFlash());

const mongoStore = connectMongo(expressSession);

app.use(
  expressSession({
    secret: "secrate",
    store: new mongoStore({
      mongooseConnection: mongoose.connection
    })
  })
);

mongoose.set("useFindAndModify", false);

app.use(express.static("public"));
app.use(expressEdge);
app.use(fileUpload());

app.set("views", `${__dirname}/views`);

app.use("*", (req, res, next) => {
  edge.global("auth", req.session.userId);
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const storePost = require("./middleware/storePost");
const auth = require("./middleware/auth");
const redirectIdAuthenticated = require("./middleware/redirectIfAuthenticated");

app.get("/auth/login", redirectIdAuthenticated, loginController);
app.post("/users/login", redirectIdAuthenticated, loginUserController);
app.get("/auth/register", redirectIdAuthenticated, createUserController);
app.post("/users/register", redirectIdAuthenticated, storeUserController);
app.get("/", homePageController);

app.get("/post/:id", getPostController);
app.get("/posts/new", auth, createPostController);
app.post("/posts/store", auth, storePost, storePostController);

app.get("/auth/logout", redirectIdAuthenticated, logoutController);

app.listen(4000, () => {
  console.log("App listning at 4000");
});
