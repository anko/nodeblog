const bcrypt = require("bcrypt");
const mongoos = require("mongoose");

const UserSchema = new mongoos.Schema({
  username: {
    type: String,
    required: [true, "Please provide your name"]
  },
  email: {
    type: String,
    required: [true, "Please provide your email"],
    unique: true
  },
  password: {
    type: String,
    required: [true, "Please provide your password"]
  }
});

UserSchema.pre("save", function(next) {
  const user = this;
  bcrypt.hash(user.password, 10, function(error, encrypted) {
    user.password = encrypted;
    next();
  });
});

module.exports = mongoos.model("User", UserSchema);
