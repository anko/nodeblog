const mongoose = require("mongoose");

const Post = require("./database/models/post");

mongoose.connect(
  "mongodb://localhost/node-js-test-blog",
  { useNewUrlParser: true }
);

Post.findById("5b79acedf5ca1b6cae77cc36", (error, post) => {
  console.log(error, post);
});
// Post.find(
//   {
//     title: "My seocend blog"
//   },
//   (error, posts) => {
//     console.log(error, posts);
//   }
// );
// Post.create(
//   {
//     title: "My seocend blog",
//     description: "This is my seocend blog on mongo using nodejs",
//     content: "Lodem Ipsum"
//   },
//   (error, post) => {
//     console.log(error, post);
//   }
// );
// Post.create(
//   {
//     title: "My third blog",
//     description: "This is my third blog on mongo using nodejs",
//     content: "Lodem Ipsum"
//   },
//   (error, post) => {
//     console.log(error, post);
//   }
// );
